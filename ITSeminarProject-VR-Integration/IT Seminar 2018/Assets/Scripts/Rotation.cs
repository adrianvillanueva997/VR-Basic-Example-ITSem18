﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {

	float spinValue = 90;
    public Texture texture1,texture2;
    Renderer renderer;
    

	// Use this for initialization
	void Start () {
        renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = texture1;

        
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate (Vector3.up*spinValue*Time.deltaTime);
        

	}
	public void FlipSpin(){
        spinValue = -spinValue;
        ChangeTexture();
    }

    private void ChangeTexture()
    {
        if(renderer.material.mainTexture == texture1)
        {
            renderer.material.mainTexture = texture2;
        }
        else if(renderer.material.mainTexture == texture2)
        {
            renderer.material.mainTexture = texture1;
        }
    }
}
