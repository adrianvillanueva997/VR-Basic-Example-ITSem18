﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameController : MonoBehaviour {

    //Aqui van todos los puntos de vista que pueden estar presentes en el juego         //
	public List<POV> povs = new List<POV>();
    public GameObject player;
    public int currentPV = 0;

    //Variables for transition
	bool transitioning = false;
	public float  changeSpeed;
	POV startPOV, targetPOV;
	public MeshRenderer[] meshesTarget, meshesStart;

    public POV GetPreviousPOV()
    {
        if (currentPV == 0)
            return povs[currentPV];
        else
            return povs[currentPV-1];

    }

    public POV GetNextPOV()
    {
        if (currentPV == (povs.Count-1))
            return povs[currentPV];
        else
            return povs[currentPV+1];
    }

    public void StartTransition (float sizeDifference) {
		
		//Transparent
		transitioning = true;		
		//Activate fade-in objects and set transparency to 0.
		targetPOV.gameObject.SetActive(true);
		//Save MeshRenderers of children of target and start POV
		meshesStart = startPOV.gameObject.GetComponentsInChildren<MeshRenderer>();
		meshesTarget = targetPOV.gameObject.GetComponentsInChildren<MeshRenderer>();
		foreach (MeshRenderer mesh in meshesTarget) {
			Color c = mesh.material.color;
			mesh.material.color = new Color(c.r,c.g,c.b,0.0f);
		}
		foreach (MeshRenderer mesh in meshesStart) {
			Color c = mesh.material.color;
			mesh.material.color = new Color(c.r,c.g,c.b,1.1f);
		}


    }

    public void TransitionProgress(){

		//Change object size
		if (transitioning) {

			List<GameObject> target_objects = targetPOV.elements;
			List<GameObject> start_objects = startPOV.elements;

			if (targetPOV.transform.localScale.x > startPOV.transform.localScale.x) {
				//when bigger
				targetPOV.transform.localScale *= (1 / changeSpeed);
				startPOV.transform.localScale *= (1 / changeSpeed);	
				//Fade in objects
				foreach (MeshRenderer mesh in meshesTarget) {
					Color c = mesh.material.color;				
					//(a-b)/2
					mesh.material.color = new Color (c.r, c.g, c.b,  1-startPOV.transform.localScale.x);
				}
				//Fade out 
				foreach (MeshRenderer mesh in meshesStart) {
					Color c = mesh.material.color;					
					mesh.material.color = new Color (c.r, c.g, c.b, startPOV.transform.localScale.x);
				}
				if (targetPOV.transform.localScale.x <= 1) {
					EndTransition ();
				}

			} else {
				//when smaller
				targetPOV.transform.localScale *= changeSpeed;
				startPOV.transform.localScale *= changeSpeed;	
				//Fade in objects
				foreach (MeshRenderer mesh in meshesTarget) {
					Color c = mesh.material.color;				
					//(a-b)/2
					mesh.material.color = new Color (c.r, c.g, c.b, targetPOV.transform.localScale.x);
				}
				//Fade out 
				foreach (MeshRenderer mesh in meshesStart) {
					Color c = mesh.material.color;					
					mesh.material.color = new Color (c.r, c.g, c.b, 1-targetPOV.transform.localScale.x);
				}
				if (targetPOV.transform.localScale.x >= 1) {
					EndTransition ();
				}
			
			}	
		}
	}
	void EndTransition(){
		startPOV.gameObject.SetActive (false);
		//Reset transition variables
		foreach (MeshRenderer mesh in meshesTarget) {
			Color c = mesh.material.color;
			mesh.material.color = new Color (c.r, c.g, c.b, 1);
		}
		targetPOV.transform.localScale = new Vector3 (1, 1, 1);
		transitioning = false;
	}
    void HandleInput () {
		if (Input.GetKeyDown (KeyCode.A)) {
			targetPOV = GetPreviousPOV ();
			startPOV = povs [currentPV];
			if (targetPOV !=startPOV) {
				print ("Transitioning to the previous POV");
				targetPOV.transform.localScale = new Vector3 (1 / targetPOV.differenceWithNext,1 / targetPOV.differenceWithNext,1 / targetPOV.differenceWithNext);
				StartTransition (targetPOV.differenceWithNext);
				currentPV--;
			}else
				print ("Already at the first POV");
		}
		if(Input.GetKeyDown(KeyCode.D)){
			targetPOV = GetNextPOV();
			startPOV = povs [currentPV];
			if(targetPOV != povs[currentPV]){
				print ("Transitioning to the next POV");
				Debug.Log (startPOV.gameObject.name +" "+ startPOV.differenceWithNext);
				targetPOV.transform.localScale = new Vector3 (startPOV.differenceWithNext,startPOV.differenceWithNext,startPOV.differenceWithNext);
				StartTransition (startPOV.differenceWithNext);
				currentPV++;
			}else
				print("Already at the last POV");
   		}
		if(Input.GetKeyDown(KeyCode.W)){
			transform.root.localScale += new Vector3(3,3,3);
   		}
		if(Input.GetKeyDown(KeyCode.S)){
			transform.root.localScale -= new Vector3(3,3,3);
   		}
    }

    // Use this for initialization
    void Start()
    {
		startPOV = povs [currentPV];
    	//Activate items visible in starting POV
		startPOV.gameObject.SetActive(true); 
    }

    // Update is called once per frame
    void Update()
    {
    	if (transitioning){
			TransitionProgress();
    	}
    	else{
    		HandleInput();
    	}
    }
}