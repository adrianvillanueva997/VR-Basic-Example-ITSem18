﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class POV : MonoBehaviour
{

    public List<GameObject> elements;
    public float  differenceWithNext;

    void Awake(){


		foreach (Transform child in transform) {
			GameObject go = child.gameObject;
			elements.Add (go);
		}
		gameObject.SetActive (false);

    }

}